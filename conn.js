'use strict';
const Pool = require('pg').Pool
const pool = new Pool({
  user: 'project1',
  host: '192.168.99.101',
  database: 'project1',
  password: 'terserah',
  port: 5432,
})

const getUsers = (request, response) => {
  pool.query('SELECT * FROM project1."Produk" ORDER BY id ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
};

const findUsers = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM project1."Produk" WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createUsers = (request, response) => {
  const { nama, harga } = request.body

  pool.query('INSERT INTO project1."Produk" (nama, harga) VALUES ($1, $2)', [nama, harga], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send(`User added with ID: ${results.insertId}`)
  })
}

const updateUsers = (request, response) => {
  const id = parseInt(request.params.id)
  const { nama, harga } = request.body

  pool.query(
    'UPDATE project1."Produk" SET nama = $1, harga = $2 WHERE id = $3',
    [nama, harga, id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`User modified with ID: ${id}`)
    }
  )
}

const deleteUsers = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('DELETE FROM project1."Produk" WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`User deleted with ID: ${id}`)
  })
}

module.exports = {
  getUsers, findUsers, createUsers, updateUsers, deleteUsers,
}